"use strict";

// Load our libraries
var express = require("express");
var path = require("path");

// instantiate an app object.
var app = express();

// setting the port as a property of the express app.
app.set("port", parseInt(process.env.APP_PORT) || parseInt(process.argv[2]) || 3000);
console.log(app.get("port"));

// define route
console.log(__dirname);
console.log(__filename);
app.use(express.static(__dirname + "/public"));
// listen to port 3000
app.listen(app.get("port"), function(){
    console.log("Server started on " + app.get("port"));
    console.log("Hoooraaayy!!");
});